package testhashmap;

import java.util.HashMap;
import java.util.Map;

public class TestHashMap {
    public static void main(String[] args) {
        Map<User, String> users = new HashMap<>();
        User one = new User(15, "TestUser");
        User two = new User(15, "Collision");
        users.put(one, "Hello");
        users.put(two, "Collision");
        System.out.println(one.hashCode() == two.hashCode());
        System.out.println(one.equals(two));
        System.out.println(one.equals(one));
        System.out.println(users.get(one));
        System.out.println(users.get(two));

    }
}
