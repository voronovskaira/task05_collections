package priorityqueue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main {
    private static Logger LOG = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        CustomPriorityQueue<Integer> test = new CustomPriorityQueue<>();
        test.add(6);
        test.add(3);
        test.add(1);
        test.add(9);
        LOG.info(test.poll());
        LOG.info(test.poll());
        LOG.info(test.poll());
        LOG.info(test.poll());
        LOG.info(test.poll());









    }
}
