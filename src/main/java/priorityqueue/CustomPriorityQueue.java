package priorityqueue;

import java.util.*;

public class CustomPriorityQueue<T extends Comparable<T>> implements Queue<T> {

    private List<T> elements;
    private Comparator<T> comparator;

    public CustomPriorityQueue() {
        this.elements = new ArrayList<>();
    }

    public CustomPriorityQueue(Comparator<T> comparator) {
        this();
        this.comparator = comparator;
    }

    @Override
    public boolean add(T o) {
        return offer(o);
    }

    @Override
    public boolean offer(T o) {
        if (o == null)
            throw new NullPointerException();
        return elements.add(o);
    }

    @Override
    public T remove() {
        return elements.remove(elements.indexOf
                (comparator == null ? Collections.min(elements) : Collections.min(elements, comparator)));
    }

    @Override
    public T poll() {
        if (size() == 0)
            return null;
        else {
            T element = comparator == null ? Collections.min(elements) : Collections.min(elements, comparator);
            elements.remove(element);
            return element;
        }
    }

    @Override
    public T element() {
        if (peek() == null)
            throw new NoSuchElementException();
        else
            return peek();
    }

    @Override
    public T peek() {
        if (size() == 0)
            return null;
        else {
            return comparator == null ? Collections.min(elements) : Collections.min(elements, comparator);
        }
    }

    @Override
    public int size() {
        return elements.size();
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public boolean contains(Object o) {
        return elements.contains(o);
    }

    @Override
    public Iterator<T> iterator() {
        return elements.iterator();
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public Object[] toArray(Object[] a) {
        return elements.toArray();
    }

    @Override
    public boolean remove(Object o) {
        return elements.remove(o);
    }

    @Override
    public boolean containsAll(Collection c) {
        return elements.containsAll(c);
    }

    @Override
    public boolean addAll(Collection c) {
        return elements.addAll(c);
    }

    @Override
    public boolean removeAll(Collection c) {
        return elements.retainAll(c);
    }

    @Override
    public boolean retainAll(Collection c) {
        return elements.retainAll(c);
    }

    @Override
    public void clear() {
        elements.clear();
    }
}
