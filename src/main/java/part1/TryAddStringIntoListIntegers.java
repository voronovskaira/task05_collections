package part1;

import java.util.ArrayList;
import java.util.List;

public class TryAddStringIntoListIntegers {
    public static void main(String[] args) {
        addStringIntoListOfIntegers();
    }

    public static void addToList(List list) {//if List<Integer> list the compiler will prevent from adding Strings
        list.add(7);
        list.add("Hello");
    }

    public static void addStringIntoListOfIntegers() {
        List<Integer> listInt = new ArrayList<>();
        addToList(listInt);
        listInt.forEach(System.out::println);
    }
}
