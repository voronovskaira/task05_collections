package part1.generic;

import java.util.ArrayList;
import java.util.List;

public class Test {
    public static void main(String[] args) {
        Ship<Droid> ship = new Ship<>();
        List<BattleDroid> droids = new ArrayList<>();
        droids.add(new GeneralDroid());
        droids.add(new BattleDroid());
        //droids.add(new Droid());
        ship.putBattleDroidsOnShip(droids);
        ship.print();
    }
}
