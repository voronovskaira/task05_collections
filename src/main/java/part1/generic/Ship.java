package part1.generic;

import java.util.ArrayList;
import java.util.List;

public class Ship<T extends Droid> {

    private List<T> ship = new ArrayList<>();


    T put(T droid) {
        return ship.add(droid) ? droid : null;
    }

    T get(T droid) {
        return ship.get(ship.indexOf(droid));
    }

    void putBattleDroidsOnShip(List<? extends BattleDroid> list){
        for(BattleDroid droid: list){
            ship.add((T) droid);
        }
    }

    void print(){
        for (T droid: ship)
            System.out.println(droid);
    }

}
