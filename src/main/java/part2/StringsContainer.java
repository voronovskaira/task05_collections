package part2;

import java.util.Arrays;
import java.util.NoSuchElementException;

public class StringsContainer {
    private String[] array;
    private final int DEFAULT_CAPACITY = 10;
    private int currentIndex = 0;


    public StringsContainer() {
        this.array = new String[DEFAULT_CAPACITY];
    }

    public void add(String str) {
        if (currentIndex >= array.length * 0.75) {
            String[] array2 = Arrays.copyOf(array, array.length * 2);
            array = array2;
        } else {
            array[currentIndex] = str;
            currentIndex++;
        }
    }

    public String get(int index) {
        if(index>=currentIndex){
            throw new NoSuchElementException();
        }
        return array[index];
    }

    public static void main(String[] args) {
        StringsContainer container = new StringsContainer();
        container.add("Hello");
        System.out.println(container.get(1));
    }
}
