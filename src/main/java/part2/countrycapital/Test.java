package part2.countrycapital;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Test {
    public static void main(String[] args) {
        CountryCapital one = new CountryCapital("Ukraine", "Kiev");
        CountryCapital two = new CountryCapital("Poland", "Warsaw");
        CountryCapital three = new CountryCapital("Italy", "Rome");
        CountryCapital four = new CountryCapital("Austria", "Vienna");
        List<CountryCapital> counties = new ArrayList<>();
        counties.add(one);
        counties.add(two);
        counties.add(three);
        counties.add(four);
//        System.out.println(counties);
//        Collections.sort(counties);
//        System.out.println(counties);
//        Collections.sort(counties, new CapitalComparator());
//        System.out.println(counties);

        CountryCapital[] arr1 = new CountryCapital[4];
        arr1[0] = one;
        arr1[1] = two;
        arr1[2] = three;
        arr1[3] = four;
        for (CountryCapital a : arr1) {
            System.out.println(a);
        }
        Arrays.sort(arr1, new CapitalComparator());
        for (CountryCapital a : arr1) {
            System.out.println(a);

        }
    }
}
