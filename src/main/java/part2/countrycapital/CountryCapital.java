package part2.countrycapital;

public class CountryCapital implements Comparable<CountryCapital>{

    private String country;
    private String capital;

    public CountryCapital(String country, String capital) {
        this.country = country;
        this.capital = capital;
    }

    public String getCountry() {
        return country;
    }

    @Override
    public String toString() {
        return "CountryCapital{" +
                "country='" + country + '\'' +
                ", capital='" + capital + '\'' +
                '}';
    }

    @Override
    public int compareTo(CountryCapital o) {
        return country.compareTo(o.getCountry());
    }

    public String getCapital() {
        return capital;
    }
}
