package part2.countrycapital;

import java.util.Comparator;

public class CapitalComparator implements Comparator<CountryCapital> {

    @Override
    public int compare(CountryCapital o1, CountryCapital o2) {
        return o1.getCapital().compareTo(o2.getCapital());
    }
}
