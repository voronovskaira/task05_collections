package owndeque;

import java.util.*;

public class CustomDeque<T> implements Deque<T> {
    private Node tail;
    private Node head;
    private int size = 0;

    private class Node {
        T value;
        Node previous;
        Node next;

        Node(Node previous, T value, Node next) {
            this.value = value;
            this.previous = previous;
            this.next = next;
        }
    }

    @Override
    public boolean offer(T o) {
        return add(o);
    }

    @Override
    public T remove() {
        if (Objects.isNull(head)) {
            throw new NoSuchElementException();
        }
        return poll();
    }

    @Override
    public T poll() {
        if (Objects.isNull(head)) {
            return null;
        }
        T temp = head.value;
        head = head.next;
        size--;
        return temp;
    }

    @Override
    public T element() {
        if (Objects.isNull(head)) {
            throw new NoSuchElementException();
        }
        return head.value;
    }

    @Override
    public T peek() {
        if (Objects.isNull(head)) {
            return null;
        }
        return head.value;
    }

    @Override
    public void push(T t) {
        addFirst(t);
    }

    @Override
    public T pop() {
        return poll();
    }

    @Override
    public void addFirst(T t) {
        Node l = head;
        Node newNode = new Node(null, t, l);
        head = newNode;
        if (Objects.isNull(l))
            tail = newNode;
        else
            l.previous = newNode;
        size++;
    }

    @Override
    public void addLast(T t) {
        add(t);
    }

    @Override
    public boolean offerFirst(T t) {
        return false;
    }

    @Override
    public boolean offerLast(T t) {
        return add(t);
    }

    @Override
    public T removeFirst() {
        if (Objects.isNull(head)) {
            throw new NoSuchElementException();
        }
        return poll();
    }

    @Override
    public T removeLast() {
        if (Objects.isNull(tail))
            throw new NoSuchElementException();
        return pollLast();
    }

    @Override
    public T pollFirst() {
        return poll();
    }

    @Override
    public T pollLast() {
        T temp = tail.value;
        tail = tail.previous;
        size--;
        return temp;
    }

    @Override
    public T getFirst() {
        return peek();
    }

    @Override
    public T getLast() {
        return peekLast();
    }

    @Override
    public T peekFirst() {
        return peek();
    }

    @Override
    public T peekLast() {
        return tail.value;
    }

    @Override
    public boolean removeFirstOccurrence(Object o) {
        return false;
    }

    @Override
    public boolean removeLastOccurrence(Object o) {
        return false;
    }

    @Override
    public boolean add(T o) {
        if (Objects.isNull(o)) {
            throw new NullPointerException();
        }
        Node l = tail;
        Node newNode = new Node(l, o, null);
        tail = newNode;
        if (Objects.isNull(l))
            head = newNode;
        else
            l.next = newNode;
        size++;
        return true;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }


    @Override
    public int size() {
        return 0;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public Iterator iterator() {
        return null;
    }

    @Override
    public Iterator<T> descendingIterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public Object[] toArray(Object[] a) {
        return new Object[0];
    }

    @Override
    public boolean addAll(Collection c) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public boolean retainAll(Collection c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection c) {
        return false;
    }

    @Override
    public boolean containsAll(Collection c) {
        return false;
    }


}
